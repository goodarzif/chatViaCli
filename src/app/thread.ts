import {Message} from "./message";
import {uuid} from "./util/uuid";
export class Thread {
  id: string;
  name: string;
  avatarSrc: string;
  lastMessage: Message;
  constructor(id?: string, name?: string, avatarSrc?: string) {
    this.id = id || uuid();
    this.name = name || null;
    this.avatarSrc = avatarSrc || null;
  }
}
