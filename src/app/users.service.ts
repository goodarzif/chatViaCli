import { Injectable } from '@angular/core';
import {User} from "./user";
import {BehaviorSubject, Subject} from "rxjs";

@Injectable()
export class UsersService {
  currentUser: Subject<User> = new BehaviorSubject<User>(null);
  constructor() {
  }
  public setCurrentUser(newUser: User): void {
    this.currentUser.next(newUser);
  }
}

export const userServiceInjectable: Array<any> = [
  UsersService
];

