import { Component } from '@angular/core';
import {MessagesService} from "./messages.service";
import {ThreadService} from "./thread.service";
import {UsersService} from "./users.service";
import {ChatExampleData} from "./data/chat-example-data";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public messagesService: MessagesService,
              public threadService: ThreadService,
              public userService: UsersService) {
    ChatExampleData.init(messagesService,threadService,userService);
  }
}
