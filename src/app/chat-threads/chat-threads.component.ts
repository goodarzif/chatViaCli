import { Component, OnInit } from '@angular/core';
import {ThreadService} from "../thread.service";
import {Observable} from "rxjs";
import {Thread} from "../thread";

@Component({
  selector: 'app-chat-threads',
  templateUrl: './chat-threads.component.html',
  styleUrls: ['./chat-threads.component.css']
})
export class ChatThreadsComponent implements OnInit {
    threads: Observable<Thread[]>;
  constructor(public threadService: ThreadService) {
    this.threads = threadService.orderedThreads;
  }

  ngOnInit() {
  }

}
