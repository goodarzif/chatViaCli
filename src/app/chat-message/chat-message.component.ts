import {Component, OnInit, Input} from '@angular/core';
import {User} from "../user";
import {Message} from "../message";
import {UsersService} from "../users.service";

@Component({
  selector: 'app-chat-message',
  templateUrl: './chat-message.component.html',
  styleUrls: ['./chat-message.component.css']
})
export class ChatMessageComponent implements OnInit {
  currentUser: User;
  incoming: boolean;
  @Input() message: Message;
  constructor(public usersService: UsersService) {
  }

  ngOnInit() {
    this.usersService.currentUser.subscribe((user: User)=>{
      this.currentUser = user;
      if(this.message.author && user)
        this.incoming = this.message.author.id !== user.id;
    });
  }

}
