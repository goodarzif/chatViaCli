import {Component, OnInit, ElementRef} from '@angular/core';
import {Message} from "../message";
import {Observable} from "rxjs";
import {User} from "../user";
import {Thread} from "../thread";
import {ThreadService} from "../thread.service";
import {UsersService} from "../users.service";
import {MessagesService} from "../messages.service";

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.css']
})
export class ChatWindowComponent implements OnInit {
  messages: Observable<Message[]>;
  currentUser: User;
  currentThread: Thread;
  draftMessage: Message = new Message();
  constructor(public threadService: ThreadService,
              public usersService: UsersService,
              public messagesService: MessagesService,
              public el:ElementRef) { }

  ngOnInit() {
    this.messages = this.threadService.currentThreadMessages;
    this.usersService.currentUser.subscribe((user: User)=>{
      this.currentUser = user;
    });
    this.threadService.currentThread.subscribe((thread: Thread)=>{
      this.currentThread = thread;
    });
    this.messages.subscribe((messages: Message[])=>{
      setTimeout(()=>{
        this.scrollToBottom();
      });
    });
  }
  onEnter(event): void{
    this.sendMessage();
    event.preventDefault();
  }

  sendMessage(): void {
    const m:Message = this.draftMessage;
    m.author = this.currentUser;
    m.isRead = true;
    m.thread = this.currentThread;
    this.messagesService.addMessage(m);
    this.draftMessage = new Message();
  }

  scrollToBottom(): void {
    const scrollPane: any = this.el.nativeElement.querySelector(".msg-container-base");
    scrollPane.scrollTop = scrollPane.scrollHeight;
  }
}
