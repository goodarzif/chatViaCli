import {User} from "./user";
import {Thread} from "./thread";
import {uuid} from "./util/uuid";
export class Message {
  id: string;
  text: string;
  sentAt: Date;
  author: User;
  isRead: boolean;
  thread: Thread;
  constructor(obj?: any) {
    this.id = obj && obj.id || uuid();
    this.text = obj && obj.text || null;
    this.sentAt = obj && obj.sentAt || new Date();
    this.author = obj && obj.author || null;
    this.isRead = obj && obj.isRead || false;
    this.thread = obj && obj.thread || null;
  }
}
