import { Injectable } from '@angular/core';
import {Message} from "./message";
import {Subject, Observable} from "rxjs";
import {Thread} from "./thread";
import {User} from "./user";
const initalMessages: Array<Message> = [];
interface IMessageOperation extends  Function {
  (messages: Message[]): Message[];
}
@Injectable()
export class MessagesService {
  newMessages: Subject<Message> = new Subject<Message>();
  messages: Observable<Message[]> ;
  create: Subject<Message> = new Subject<Message>();
  updates: Subject<any> = new Subject<any>();
  markThreadAsRead: Subject<any> = new Subject<any>();
  constructor() {
    this.messages = this.updates.scan((messages: Message[],operation: IMessageOperation)=>{
      return operation(messages);
    },initalMessages).publishReplay(1).refCount();

    this.create.map(function(message: Message):IMessageOperation{
      return (messages:Message[])=>{
        return messages.concat(message);
      };
    }).subscribe(this.updates);
    this.newMessages.subscribe(this.create);

    this.markThreadAsRead.map((thread: Thread)=>{
      return (messages: Message[])=>{
        return messages.map((message: Message)=>{
          if(message.thread.id == thread.id) {
            message.isRead = true;
          }
          return message;
        });
      };
    }).subscribe(this.updates);
  }
  addMessage(message: Message):void {
    this.newMessages.next(message);
  }

  messagesForThreadUser(thread: Thread, user: User): Observable<Message> {
    return this.newMessages.filter((message: Message)=>{
      return (message.thread.id === thread.id &&
                message.author.id !== user.id );
    });
  }
}


export const messagesServiceInjectable: Array<any> = [ MessagesService ];

